const mix = require('laravel-mix');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

mix.webpackConfig({
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: file => (
                    /node_modules/.test(file) &&
                    !/\.vue\.js/.test(file)
                )
            }
        ],
    },
    plugins: [
        new VueLoaderPlugin()
    ]
});

mix.js('resources/js/app.js', 'public/js/app.js')
   .sass('resources/sass/app.scss', 'public/css');
