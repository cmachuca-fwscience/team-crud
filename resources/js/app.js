// import core vue app
import Vue from "vue";
import VueRouter from "vue-router";
import VueAxios from 'vue-axios';
import axios from 'axios';
// import ui components
import BootstrapVue from 'bootstrap-vue'
import VueSweetAlert from 'vue-sweetalert'
// importing pages for routes
import mainComponent from './layout/mainLayout';
import home from './pages/home';
import players from './pages/players';
import teams from './pages/teams';

import App from './App.vue';

Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueSweetAlert);

const routes = [{
    path: '/',
    component: mainComponent,
    redirect: '/home'
    },{
        path: '/home',
        component: home
    },
    {
        name: "players",
        path: '/players',
        component: players
    },
    {
        name: "teams",
        path: '/teams',
        component: teams
    },
];

const router = new VueRouter({
    routes,
    linkActiveClass: 'active',
    props: true
});

const app = new Vue({
    el: '#app',
    render: h => h(App),
    router,
    data: {
        global_url: 'http://localhost/'
    }
});