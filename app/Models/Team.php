<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['name'];

    protected $visible = ['id', 'name', 'players'];

    public function players()
    {
        return $this->hasMany(Player::class);
    }
}
