<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $fillable = ['first_name', 'last_name', 'team_id'];

    protected $visible = ['id', 'first_name', 'last_name', 'team_id', 'created_at', 'playerOf'];

    public function playerOf()
    {
        return $this->belongsTo('App\Models\Team', 'team_id', 'id');
    }
}
